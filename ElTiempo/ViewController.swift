//
//  ViewController.swift
//  ElTiempo
//
//  Created by Otto Colomina Pardo on 2/10/16.
//  Copyright © 2016 Universidad de Alicante. All rights reserved.
//

import UIKit
import Bond

class ViewController: UIViewController, UITextFieldDelegate {
    let tiempoModelo = TiempoModelo()

    @IBOutlet weak var estadoLabel: UILabel!    
    @IBOutlet weak var estadoImagen: UIImageView!
    @IBOutlet weak var localidadField: UITextField!
    let viewModel = TiempoViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //consultarTiempo(localidad: "Madrid")
        localidadField.delegate = self
        estadoLabel.text = self.viewModel.estado.value
        self.viewModel.icono
            .filter {
                //Si la URL es "" la ignoramos
                icono in
                return icono != ""
            }
            .map {
                //Transformamos de String -> clase URL -> datos binarios -> UIImage
                icono in
                let datosIcono = try! Data(contentsOf:URL(string:icono)!)
                let imgIcono = UIImage(data: datosIcono)
                return imgIcono!
        }.bind(to: self.estadoImagen.reactive.image)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func consultarTiempoPulsado(_ sender: AnyObject) {
        if let loc = self.localidadField.text {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            self.viewModel.consultarTiempo(de: loc)
            self.viewModel.estado.bind(to: self.estadoLabel.reactive.text)
        }
        
    }
    
    
}

