//
//  TiempoViewModel.swift
//  ElTiempo
//
//  Created by Master Móviles on 16/02/2017.
//  Copyright © 2017 Universidad de Alicante. All rights reserved.
//

import Foundation
import Bond

class TiempoViewModel {
    let modelo = TiempoModelo()
    let estado = Observable<String>("")
    let icono = Observable<String>("")
    
    func consultarTiempo(de localidad : String) {
        //AQUI
        modelo.consultarTiempo(localidad: localidad){
            estado,icono in
            self.estado.value = estado
            self.icono.value = icono
        }
//        1. LLama a consultarTiempo del modelo. Tiene dos parámetros, la localidad y una clausura con dos parámetros (estado e icono), que se ejecutará como un *callback* cuando el servidor devuelva el estado del tiempo. Puedes ver en el fuente del modelo más detalles.
//        2. Dentro de esa clausura actualiza el observable estado con el valor del primer parámetro. Más tarde nos ocuparemos del icono
    }
}
